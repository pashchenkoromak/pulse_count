import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
import threading
import time

CAMERA_INDEX = 0
WINDOW_SIZE = None # 2 x window_size * 2x window_size
RECORD_LENGTH = None # length of the recording
frames = None # store frames
frame_counter = None # amount of frames
processed_frame_counter = None # amount of processed frames
capturer_mutex = None
capturer = cv.VideoCapture(CAMERA_INDEX)
max_frame = 300

def capture_video():
    capturer_mutex.acquire()    
    fps = int(capturer.get(cv.CAP_PROP_FPS))
    print("fps: ", fps)
    counter = RECORD_LENGTH * fps
    global frame_counter

    while counter > frame_counter:
        # Capture frame-by-frame
        ret, frame = capturer.read()

        frames.append(frame)
        frame_counter += 1
        capturer_mutex.release()

        if cv.waitKey(1) & 0xFF == ord('q'):
            break
        capturer_mutex.acquire()
    capturer_mutex.release()

def GetNextFrame():
    frame = None
    capturer_mutex.acquire()
    global processed_frame_counter
#    print("Using frame with index ", processed_frame_counter)
    if processed_frame_counter > frame_counter - 1:
        capturer_mutex.release()
        return None

    processed_frame_counter += 1
    frame = frames[processed_frame_counter - 1]
    capturer_mutex.release()
    return frame

processed_frame_counter = 0
def find_points(frame):
    v_size = len(frame)
    h_size = len(frame[0])
    return (v_size/2, h_size/2)
        
    center_part = sum(sum(sum(frame[int(v_size/2)-window_size:int(v_size/2)+window_size,int(h_size/2)-window_size:int(h_size/2)+window_size,2:3].astype(int))))
    arr.append(center_part.astype(float)/(window_size*window_size))
    cv.rectangle(frame, (int(h_size/2)-window_size,int(v_size/2)-window_size), (int(h_size/2)+window_size,int(v_size/2)+window_size), (255,0,0))
    # Display the resulting frame
#    print("One more frame processed")

def Init():
    global CAMERA_INDEX 
    CAMERA_INDEX = 0
    global WINDOW_SIZE
    WINDOW_SIZE = 30; # 2 x window_size * 2x window_size
    global RECORD_LENGTH
    RECORD_LENGTH = 10 # length of the recording
    global frames
    frames = [] # store frames
    global frame_counter 
    frame_counter = 0 # amount of frames
    global processed_frame_counter
    processed_frame_counter = 0 # amount of processed frames
    global capturer_mutex
    capturer_mutex = threading.Lock()

def main():
    Init()
    print(threading.current_thread().__class__.__name__)
    videoCapturing = threading.Thread(target=capture_video)
    videoCapturing.start()
    videoCapturing.join()
    while processed_frame_counter < max_frame:
        frame = GetNextFrame()
        if frame is None:
            return
    
        points = find_points(frame)
        
        cv.imshow('frame',frames[processed_frame_counter - 1])
        time.sleep(1/30)
        if cv.waitKey(1) & 0xFF == ord('q'):
            break



    
    #plt.plot(np.arange(1,length, 1/fps),arr[fps:])
    # When everything done, release the capture
    capturer.release()
    cv.destroyAllWindows()

    #plt.show()


if __name__ == "__main__":
    main()
